#/bin/bash

# Get current timestamp
timestamp=$(date +"%Y%m%d%H%M%S")

# Create a backup directory
backup_dir="/root/gitbackup/$timestamp"
mkdir -p "$backup_dir"

# Backup Docker container
#docker export gitlab > "$backup_dir/gitlab.tar"

#Create a docker image backup
docker save gitlab/gitlab-ce | gzip > "$backup_dir/gitlab.tar.gz"
# Backup Docker volumes
volumes=$(docker inspect --format='{{range $vol := .Mounts}}{{$vol.Source}}:{{$vol.Destination}} {{end}}' gitlab)
for volume in $volumes; do
    source=$(echo "$volume" | cut -d':' -f1)
    destination=$(echo "$volume" | cut -d':' -f2)
    volume_name=$(basename "$source")

    echo "Backing up volume: $volume_name"
    tar -czf "$backup_dir/$volume_name.tar.gz" -C "$source" .
done

echo "Backup complete! Files are stored in: $backup_dir"

