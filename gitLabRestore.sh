#!/bin/bash

#make git lab config, data and log directory baesd off path of the gitlab install script
#run this script in the back up folder of gitlab files.  should be four files
#config.tar.gz, data.tar.gz, gitlab.tar, logs.tar.gz

#stop gitlab if running
docker stop gitlab

#Make directories and parent directories as needed
sudo mkdir -p /srv/gitlab/config
sudo mkdir -p /srv/gitlab/logs
sudo mkdir -p /srv/gitlab/data

#extract 
tar -zxf config.tar.gz --directory /srv/gitlab/config
tar -zxf data.tar.gz --directory /srv/gitlab/data
tar -zxf logs.tar.gz --directory /srv/gitlab/logs

#restore the container.  Still need to commit to create an image of the restored container
docker load -i gitlab.tar.gz

docker start gitlab

docker stats gitlab
