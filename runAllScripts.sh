#!/bin/bash

# install vim
bash ./updateAndVimInstall.sh

# remmove runc and dependancies and install docker
bash ./dockerInstall.sh

# Git lab docker install
bash ./gitLabRunnerInstall.sh
