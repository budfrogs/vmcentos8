#!/bin/bash

# Install GitLab Docker image
sudo docker pull gitlab/gitlab-ce:latest

# Create directories for GitLab configuration and data
sudo mkdir -p /srv/gitlab/config
sudo mkdir -p /srv/gitlab/logs
sudo mkdir -p /srv/gitlab/data

# Run GitLab container
#sudo docker run -d --hostname gitlab.example.com \
 docker run --detach \
  -p 443:443 -p 80:80 -p 1022:22 \
  --name gitlab \
  --restart always \
  -v /srv/gitlab/config:/etc/gitlab \
  -v /srv/gitlab/logs:/var/log/gitlab \
  -v /srv/gitlab/data:/var/opt/gitlab \
  --shm-size 2gb \
  gitlab/gitlab-ce:latest

echo "GitLab installation complete!"
#run url: port:80 ex: 10.0.0.57:80
#To get the default root password
#docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
