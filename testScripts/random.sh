#!/usr/bin/bash

for i in {1..50}; do
   echo "Iteration $i"

    # Generate random delay between 0 and 120 seconds (2 minutes)
    delay=$(( RANDOM % 301 ))

    sleep $delay

    #echo "Uptime before stress:"
    uptime

    #echo "Executing stress command..."
    #stress --cpu 8 --timeout 20
    stress --cpu 4 --vm 2 --io 1 -t 120

    #echo "Uptime after stress:"
    uptime

    echo "----------------------------------------"
done
