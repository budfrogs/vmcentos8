# vmcentos8

# DEVOPS training

Training to develop scripts to build test and integrat projects based on contract requirments.

## Description

This repository is a group of scripts for setting up a devops enviroment. It includes Gitlab install, docker install, gitlab runner, centos repo. It is a contiuous traing eveloution to support an Analytica development enviroment that can be transfered to a Government site in a package format.

---

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/budfrogs/vmcentos8.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/budfrogs/vmcentos8/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## List of tasks
- [X] Setup Gitlab on one vm
- [X] Setup Gitlab runner on second vm and ensure it runs basick yaml file in gitlab repo
- [ ] Create a script to set up a Centos repository. Set up two VM's.  VM1 should have centos Repo downlaoded and configured to act as a repo.  VM2 should be configured to use VM1 as the repo for centos updates! 
- [ ] Next task to be assigned

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

## Visuals

Add gifs as needed

## Installation

Various scripts are set up to run from one script. Performing that actions from start to finish. Other scripts are all the commands to complete the task. Some portions of those scripts can be run sequentially other protions need to be run seperatly.

## Usage

Most scripts have detailed notes inside the script to explain "how to" or "what if" situations for various installation differences.

## Support

Contact James.Smith@analytica.net for support on any of scripts related to this project.

## Roadmap

N/A

## Contributing

Contributions and suggestions are welcome.

## Authors and acknowledgment

Provided via traing with Hung Nguyen.

## License

For open source projects, say how it is licensed.

## Project status

N/A
