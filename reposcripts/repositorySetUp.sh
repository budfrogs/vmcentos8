
# VM1
# each command in this script should run individually or break the script out into individual scripts that run each section before moving on to the next section.

# Set up on VM1 where repository will reside
# 1. sudo dnf install createrepo_c httpd -y
sudo dnf install createrepo_c httpd -y

# 2 Create the directory for your mirror. You can create this wherever you want. For this example, we'll put it in the /var/www/html directory:
# Directory /home is used to space limitations size of /var/www/html
# Original command >>> sudo mkdir -p /var/www/html/repos/centos/8/BaseOS/x86_64/os/
sudo mkdir -p /home/www/html/repos/centos-stream/8/AppStream/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/BaseOS/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/Devel/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/HighAvailability/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/NFV/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/PowerTools/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/RT/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/ResilientStorage/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/centosplus/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/cloud/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/core/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/cr/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/extras/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/hyperscale/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/kmods/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/messaging/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/nfv/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/opstools/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/storage/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/virt/x86_64/


# 3. Download the packages. You will need to download the packages from the CentOS repositories to your mirror. You can use rsync for this:
# Only certain mirrow sites allow rsync to run.
# If downloading fresh you will be limited to the download rate of two things.  Either the network you reside on or the mirror you get your information from.
# Based on size I have broken the 20 different directories for the repo up based on size of the packages.  You can either download all in order by just running
# all twenty in one script or create four scripts and run them each in their own terminal window to decrease the time of downloading for roughly 3hrs to 1hr.
# Original command >>> sudo rsync -avz --progress rsync://mirror.centos.org/centos/8/BaseOS/x86_64/ /var/www/html/repos/centos/8/BaseOS/x86_64/

#Option 1 open four terminal windows for downloading 151gb quicker.

# terminal 1
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/AppStream/x86_64/ /home/www/html/repos/centos-stream/8/AppStream/x86_64/

# terminal 2
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/BaseOS/x86_64/ /home/www/html/repos/centos-stream/8/BaseOS/x86_64/

# terminal 3
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/PowerTools/x86_64/ /home/www/html/repos/centos-stream/8/PowerTools/x86_64/

# terminal 4
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/storage/x86_64/ /home/www/html/repos/centos-stream/8/storage/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/virt/x86_64/ /home/www/html/repos/centos-stream/8/virt/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/RT/x86_64/ /home/www/html/repos/centos-stream/8/RT/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/ResilientStorage/x86_64/ /home/www/html/repos/centos-stream/8/ResilientStorage/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/centosplus/x86_64/ /home/www/html/repos/centos-stream/8/centosplus/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cloud/x86_64/ /home/www/html/repos/centos-stream/8/cloud/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/core/x86_64/ /home/www/html/repos/centos-stream/8/core/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cr/x86_64/ /home/www/html/repos/centos-stream/8/cr/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/HighAvailability/x86_64/ /home/www/html/repos/centos-stream/8/HighAvailability/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/NFV/x86_64/ /home/www/html/repos/centos-stream/8/NFV/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/Devel/x86_64/ /home/www/html/repos/centos-stream/8/Devel/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/extras/x86_64/ /home/www/html/repos/centos-stream/8/extras/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/hyperscale/x86_64/ /home/www/html/repos/centos-stream/8/hyperscale/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/kmods/x86_64/ /home/www/html/repos/centos-stream/8/kmods/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/messaging/x86_64/ /home/www/html/repos/centos-stream/8/messaging/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/nfv/x86_64/ /home/www/html/repos/centos-stream/8/nfv/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/opstools/x86_64/ /home/www/html/repos/centos-stream/8/opstools/x86_64/

# centos8_stream
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/AppStream/x86_64/ /home/www/html/repos/centos-stream/8/AppStream/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/BaseOS/x86_64/ /home/www/html/repos/centos-stream/8/BaseOS/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/Devel/x86_64/ /home/www/html/repos/centos-stream/8/Devel/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/HighAvailability/x86_64/ /home/www/html/repos/centos-stream/8/HighAvailability/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/NFV/x86_64/ /home/www/html/repos/centos-stream/8/NFV/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/PowerTools/x86_64/ /home/www/html/repos/centos-stream/8/PowerTools/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/RT/x86_64/ /home/www/html/repos/centos-stream/8/RT/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/ResilientStorage/x86_64/ /home/www/html/repos/centos-stream/8/ResilientStorage/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/centosplus/x86_64/ /home/www/html/repos/centos-stream/8/centosplus/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cloud/x86_64/ /home/www/html/repos/centos-stream/8/cloud/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/core/x86_64/ /home/www/html/repos/centos-stream/8/core/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cr/x86_64/ /home/www/html/repos/centos-stream/8/cr/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/extras/x86_64/ /home/www/html/repos/centos-stream/8/extras/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/hyperscale/x86_64/ /home/www/html/repos/centos-stream/8/hyperscale/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/kmods/x86_64/ /home/www/html/repos/centos-stream/8/kmods/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/messaging/x86_64/ /home/www/html/repos/centos-stream/8/messaging/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/nfv/x86_64/ /home/www/html/repos/centos-stream/8/nfv/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/opstools/x86_64/ /home/www/html/repos/centos-stream/8/opstools/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/storage/x86_64/ /home/www/html/repos/centos-stream/8/storage/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/virt/x86_64/ /home/www/html/repos/centos-stream/8/virt/x86_64/

# 4. Create the repository metadata. After you have all the packages, you need to create the repository metadata. You can use the "createrepo_c" command for this:
# sudo createrepo_c /var/www/html/repos/centos/8/BaseOS/x86_64/os/
# ************** Note: **************
# if you move files and or change the directory paths and time after running the below command you will have to re run the command to update the sqlite db with the proper paths.
sudo createrepo_c /home/www/html/repos/centos-stream/8/

# Using teh /home directory will require it to be added to the apache conf.d file

# Add the follwing code to the end of the  /etc/httpd/conf/httpd.conf
# vim /etc/httpd/conf/httpd.conf
change DocumentRoot "/var/www/html" >> DocumentRoot "/home/www/html"

<Directory /home/www/html/repos>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

# test the file formate is correct
apachectl -t

# Change ownership
sudo chown -R apache:apache /home/www/html/repos/

# Change permissions on the directories
sudo find /home/www/html/repos/ -type d -exec chmod 755 {} \;

# change permissions on the files
sudo find /home/www/html/repos/ -type f -exec chmod 644 {} \;

sudo chcon -R -t httpd_sys_content_t /home/www/html/repos/
sudo semanage fcontext -a -t httpd_sys_content_t "/home/www/html/repos(/.*)?"
sudo restorecon -Rv /home/www/html/repos/


#5. Configure the firewall. You will need to allow HTTP traffic to your server:
sudo firewall-cmd --add-service=http --permanent
sudo firewall-cmd --reload

#6. Start and enable the HTTPD service. This will start the Apache HTTP server and set it to start on boot:
sudo systemctl start httpd
sudo systemctl enable httpd
#sudo systemctl restart httpd
#End of VM1 Setup






# VM2 
# set up on second VM2  you want to update from the repo set up above
# 1. Backup the *.repo file. CentOS 8 uses a different repo file named CentOS-Linux-BaseOS.repo. Before you make any changes, it's a good idea to backup the original file:
mkdir -p /etc/yum.repos.d/bak
cp *.repo ./bak


# 2 Edit repo files associated with your installation.
# Example, you need to edit the CentOS-Linux-AppStream.repo file to point to your new mirror.
# You will have more or less depending on the installation options you selected at the time of installation.
# You can use any text editor to do this. For this example, we'll use vim:
# comment out the mirror line by adding # to the bigging of the line >>> #mirrorlist=http://mirrorlist.centos.org/?release=$stream&arch=$basearch&repo=AppStream&infra=$infra
# change the baseurl line to repo path minus the httpd path. This path is so the dnf update can find the following directy and file "repodata/repomd.xml" which should be in the root of the baseurl. 
# example: http://10.0.0.38/repos/centos-stream/8/Name_of_Directroy_In_Repo/x86_64/os/repodata/repomd.xml
# example for one file baseurl is: baseurl=http://10.0.0.38/repos/centos-stream/8/AppStream/x86_64/os/



#sudo vim /etc/yum.repos.d/CentOS-Stream-NFV.repo
# http://10.0.0.38/repos/centos-stream/8/NFV/x86_64/os/

#sudo vim /etc/yum.repos.d/CentOS-Stream-Sources.repo


#sudo vim /etc/yum.repos.d/CentOS-Stream-AppStream.repo
# http://10.0.0.38/repos/centos-stream/8/AppStream/x86_64/os/

#sudo vim /etc/yum.repos.d/CentOS-Stream-Extras-common.repo
# Path is different http://10.0.0.38/repos/centos-stream/8/extras/x86_64/extras-common

#sudo vim /etc/yum.repos.d/CentOS-Stream-Extras.repo
# http://10.0.0.38/repos/centos-stream/8/extras/x86_64/os

#sudo vim /etc/yum.repos.d/CentOS-Stream-PowerTools.repo        
# http://10.0.0.38/repos/centos-stream/8/PowerTools/x86_64/os/

#sudo vim /etc/yum.repos.d/docker-ce.repo


#sudo vim /etc/yum.repos.d/CentOS-Stream-BaseOS.repo
# http://10.0.0.38/repos/centos-stream/8/BaseOS/x86_64/os/

#sudo vim /etc/yum.repos.d/CentOS-Stream-HighAvailability.repo
# http://10.0.0.38/repos/centos-stream/8/HighAvailability/x86_64/os/

#sudo vim /etc/yum.repos.d/CentOS-Stream-RealTime.repo 
# http://10.0.0.38/repos/centos-stream/8/RT/x86_64/os/

#sudo vim /etc/yum.repos.d/CentOS-Stream-Debuginfo.repo  

#sudo vim /etc/yum.repos.d/CentOS-Stream-Media.repo             
# used to install from media devices

#sudo vim /etc/yum.repos.d/CentOS-Stream-ResilientStorage.repo
# http://10.0.0.38/repos/centos-stream/8/ResilientStorage/x86_64/os/



#3. Clean the DNF cache. This will force DNF to re-read the repository configuration:
sudo dnf clean all

#4. Test your configuration. You can do this by trying to install a package or update your system:
sudo dnf update

# if after running update you get an error like below:

# Errors during downloading metadata for repository 'powertools':
#  - Status code: 404 for http://10.0.0.38/repos/centos-stream/8/PowerTools/x86_64/os/repodata/repomd.xml (IP: 10.0.0.38)
# Error: Failed to download metadata for repo 'powertools': Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried
# issues can be permissions to the repo folder or the path to the repo has not been added correctly to the httpd.conf file in VM1 setup

#various other commands that might be useful
#sudo  dnf repolist all
#sudo dnf config-manager --set-enabled powertools
#sudo dnf config-manager --set-disable powertools



