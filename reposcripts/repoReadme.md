# vmcentos8

# Repositories

Setting VM's up as a repository and pointing a second VM to use the repository

## Description

These scripts are for setting up a repository on one VM and configuring a second VM to access that repository. This is a basic example and directory structure with various commands to complete the configuration. Repositorie directory moved to the /home path due to space restrictions. Centos repository is 151GB this will require the httpd.conf file to be modified to include the new path.

---

## File Configuration Information

- \*.repo files are configuration files for accessing a remote repository. The mirror path is commented out and the baseUrl link is updated to match the path define for the VM containing the repository.
- \*.sh are various bash files with scripts that provide the configuration. Some parts of these bash scripts require manual editing of the configuration files in order to configure either the repository VM or the VM requesting the packages from the repository.
- /etc/httpd/conf/httpd.conf difines the paths to web files used for the repo. For this repo due to space requirements /home/www/html was used.

### Repofile Information

- When performing a linux installation repo files for the packages are created. Example: CentOS-Stream-BaseOS.repo will point to the directory path of this package. If you install Power Tools packages the file will be name CentOS-Stream-PowerTools.repo and will contain the path to the package folder for Power Tools
- Repo files will have to have their IP or domain name updated in the url based on the deplopment location

#### Example BaseOS file

- mirrorlist is commented out
- baseurl is uncommented and used.
- example code for BaseOS repo file.
```
  [baseos]
  name=CentOS Stream $releasever - BaseOS
  #mirrorlist=http://mirrorlist.centos.org/?release=$stream&arch=$basearch&repo=BaseOS&infra=$infra
  baseurl=http://10.0.0.38/repos/centos-stream/8/AppStream/x86_64/os/
  gpgcheck=1
  enabled=1
  gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
```
  #### httpd.conf File Information

  1. DocumentRoot parameter changed
  - Original = **DocumentRoot "/var/www/html"**   
  - New = **DocumentRoot "/home/www/html"**
  1. At the end of the httpd.conf file add the following code **Note** the Directory path /home/www/html/repos. Includes the folder repos.
  ```
    <Directory /home/www/html/repos>
      Options Indexes FollowSymLinks
      AllowOverride All
      Require all granted
    </Directory>
  ```

## Integrate with your tools



## Collaborate with your team



## List of tasks
- Once the location and network information is determined try to recreat a replicate offsite to ensure the enviorment can be mimiced.

## Test and Deploy
- Test by creating two VM's.  Using one VM as the repository download location.  Second VM should be used to connect to the first VM with the repository.
---

## Installation

Various scripts are set up to run from one script. Performing that actions from start to finish. Other scripts are all the commands to complete the task. Some portions of those scripts can be run sequentially other protions need to be run seperatly.

## Usage

Most scripts have detailed notes inside the script to explain "how to" or "what if" situations for various installation differences.

## Support

Contact James.Smith@analytica.net for support on any of scripts related to this project.

## Roadmap

N/A

## Contributing

Contributions and suggestions are welcome.

## Authors and acknowledgment

Provided via training with Hung Nguyen.

## License

For open source projects, say how it is licensed.

## Project status

N/A
```
