# VM2 
# set up on second VM2  you want to update from the repo set up above
# 1. Backup the *.repo file. CentOS 8 uses a different repo file named CentOS-Linux-BaseOS.repo. Before you make any changes, it's a good idea to backup the original file:
mkdir -p /etc/yum.repos.d/bak
cp *.repo ./bak


# 2 Edit repo files associated with your installation.
# Example, you need to edit the CentOS-Linux-AppStream.repo file to point to your new mirror.
# You will have more or less depending on the installation options you selected at the time of installation.
# You can use any text editor to do this. For this example, we'll use vim:
# comment out the mirror line by adding # to the bigging of the line >>> #mirrorlist=http://mirrorlist.centos.org/?release=$stream&arch=$basearch&repo=AppStream&infra=$infra
# change the baseurl line to repo path minus the httpd path. This path is so the dnf update can find the following directy and file "repodata/repomd.xml" which should be in the root of the baseurl. 
# example: http://10.0.0.38/repos/centos-stream/8/Name_of_Directroy_In_Repo/x86_64/os/repodata/repomd.xml
# example for one file baseurl is: baseurl=http://10.0.0.38/repos/centos-stream/8/AppStream/x86_64/os/



# sudo vim /etc/yum.repos.d/CentOS-Stream-NFV.repo
# http://10.0.0.38/repos/centos-stream/8/NFV/x86_64/os/
# Not used
# sudo vim /etc/yum.repos.d/CentOS-Stream-Sources.repo


# sudo vim /etc/yum.repos.d/CentOS-Stream-AppStream.repo
# http://10.0.0.38/repos/centos-stream/8/AppStream/x86_64/os/

# sudo vim /etc/yum.repos.d/CentOS-Stream-Extras-common.repo
# Path is different http://10.0.0.38/repos/centos-stream/8/extras/x86_64/extras-common

# sudo vim /etc/yum.repos.d/CentOS-Stream-Extras.repo
# http://10.0.0.38/repos/centos-stream/8/extras/x86_64/os

# sudo vim /etc/yum.repos.d/CentOS-Stream-PowerTools.repo        
# http://10.0.0.38/repos/centos-stream/8/PowerTools/x86_64/os/

# sudo vim /etc/yum.repos.d/docker-ce.repo


# sudo vim /etc/yum.repos.d/CentOS-Stream-BaseOS.repo
# http://10.0.0.38/repos/centos-stream/8/BaseOS/x86_64/os/

# sudo vim /etc/yum.repos.d/CentOS-Stream-HighAvailability.repo
# http://10.0.0.38/repos/centos-stream/8/HighAvailability/x86_64/os/

# sudo vim /etc/yum.repos.d/CentOS-Stream-RealTime.repo 
# http://10.0.0.38/repos/centos-stream/8/RT/x86_64/os/

# Not used
# sudo vim /etc/yum.repos.d/CentOS-Stream-Debuginfo.repo  

# sudo vim /etc/yum.repos.d/CentOS-Stream-Media.repo             
# used to install from media devices

# sudo vim /etc/yum.repos.d/CentOS-Stream-ResilientStorage.repo
# http://10.0.0.38/repos/centos-stream/8/ResilientStorage/x86_64/os/


#3. Clean the DNF cache. This will force DNF to re-read the repository configuration:
sudo dnf clean all

#4. Test your configuration. You can do this by trying to install a package or update your system:
sudo dnf update

# if after running update you get an error like below review VM1 configuration settings to include path to the repo in httpd.conf and file permissions.

# Errors during downloading metadata for repository 'powertools':
#  - Status code: 404 for http://10.0.0.38/repos/centos-stream/8/PowerTools/x86_64/os/repodata/repomd.xml (IP: 10.0.0.38)
# Error: Failed to download metadata for repo 'powertools': Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried
# issues can be permissions to the repo folder or the path to the repo has not been added correctly to the httpd.conf file in VM1 setup

#various other commands that might be useful
#sudo  dnf repolist all
#sudo dnf config-manager --set-enabled powertools
#sudo dnf config-manager --set-disable powertools