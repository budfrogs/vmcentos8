# VM1
# each command in this script should run individually or break the script out into individual scripts that run each section before moving on to the next section.

# Set up on VM1 where repository will reside
# 1. sudo dnf install createrepo_c httpd -y
sudo dnf install createrepo_c httpd -y

# 2 Create the directory for your mirror. You can create this wherever you want. For this example, we'll put it in the /var/www/html directory:
# Directory /home is used to space limitations size of /var/www/html
# Original command >>> sudo mkdir -p /var/www/html/repos/centos/8/BaseOS/x86_64/os/
echo "Making Centos 8 Repo Directories"
sudo mkdir -p /home/www/html/repos/centos-stream/8/AppStream/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/BaseOS/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/Devel/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/HighAvailability/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/NFV/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/PowerTools/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/RT/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/ResilientStorage/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/centosplus/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/cloud/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/core/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/cr/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/extras/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/hyperscale/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/kmods/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/messaging/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/nfv/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/opstools/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/storage/x86_64/
sudo mkdir -p /home/www/html/repos/centos-stream/8/virt/x86_64/
echo "Making docker Repo Directory"
sudo mkdir -p /home/www/html/repos/docker/linux/centos/8/x86_64/


# 3. Download the packages. You will need to download the packages from the CentOS repositories to your mirror. You can use rsync for this:
# Only certain mirrow sites allow rsync to run.
# If downloading fresh you will be limited to the download rate of two things.  Either the network you reside on or the mirror you get your information from.
# Based on size I have broken the 20 different directories for the repo up based on size of the packages.  You can either download all in order by just running
# all twenty in one script or create four scripts and run them each in their own terminal window to decrease the time of downloading for roughly 3hrs to 1hr.
# Original command >>> sudo rsync -avz --progress rsync://mirror.centos.org/centos/8/BaseOS/x86_64/ /var/www/html/repos/centos/8/BaseOS/x86_64/

#Option 1 open four terminal windows for downloading 151gb quicker.

# terminal 1
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/AppStream/x86_64/ /home/www/html/repos/centos-stream/8/AppStream/x86_64/

# terminal 2
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/BaseOS/x86_64/ /home/www/html/repos/centos-stream/8/BaseOS/x86_64/

# terminal 3
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/PowerTools/x86_64/ /home/www/html/repos/centos-stream/8/PowerTools/x86_64/

# terminal 4
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/storage/x86_64/ /home/www/html/repos/centos-stream/8/storage/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/virt/x86_64/ /home/www/html/repos/centos-stream/8/virt/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/RT/x86_64/ /home/www/html/repos/centos-stream/8/RT/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/ResilientStorage/x86_64/ /home/www/html/repos/centos-stream/8/ResilientStorage/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/centosplus/x86_64/ /home/www/html/repos/centos-stream/8/centosplus/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cloud/x86_64/ /home/www/html/repos/centos-stream/8/cloud/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/core/x86_64/ /home/www/html/repos/centos-stream/8/core/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cr/x86_64/ /home/www/html/repos/centos-stream/8/cr/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/HighAvailability/x86_64/ /home/www/html/repos/centos-stream/8/HighAvailability/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/NFV/x86_64/ /home/www/html/repos/centos-stream/8/NFV/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/Devel/x86_64/ /home/www/html/repos/centos-stream/8/Devel/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/extras/x86_64/ /home/www/html/repos/centos-stream/8/extras/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/hyperscale/x86_64/ /home/www/html/repos/centos-stream/8/hyperscale/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/kmods/x86_64/ /home/www/html/repos/centos-stream/8/kmods/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/messaging/x86_64/ /home/www/html/repos/centos-stream/8/messaging/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/nfv/x86_64/ /home/www/html/repos/centos-stream/8/nfv/x86_64/
#sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/opstools/x86_64/ /home/www/html/repos/centos-stream/8/opstools/x86_64/

# centos8_stream
echo "Downloading centos8_stream repositories"
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/AppStream/x86_64/ /home/www/html/repos/centos-stream/8/AppStream/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/BaseOS/x86_64/ /home/www/html/repos/centos-stream/8/BaseOS/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/Devel/x86_64/ /home/www/html/repos/centos-stream/8/Devel/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/HighAvailability/x86_64/ /home/www/html/repos/centos-stream/8/HighAvailability/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/NFV/x86_64/ /home/www/html/repos/centos-stream/8/NFV/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/PowerTools/x86_64/ /home/www/html/repos/centos-stream/8/PowerTools/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/RT/x86_64/ /home/www/html/repos/centos-stream/8/RT/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/ResilientStorage/x86_64/ /home/www/html/repos/centos-stream/8/ResilientStorage/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/centosplus/x86_64/ /home/www/html/repos/centos-stream/8/centosplus/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cloud/x86_64/ /home/www/html/repos/centos-stream/8/cloud/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/core/x86_64/ /home/www/html/repos/centos-stream/8/core/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/cr/x86_64/ /home/www/html/repos/centos-stream/8/cr/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/extras/x86_64/ /home/www/html/repos/centos-stream/8/extras/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/hyperscale/x86_64/ /home/www/html/repos/centos-stream/8/hyperscale/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/kmods/x86_64/ /home/www/html/repos/centos-stream/8/kmods/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/messaging/x86_64/ /home/www/html/repos/centos-stream/8/messaging/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/nfv/x86_64/ /home/www/html/repos/centos-stream/8/nfv/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/opstools/x86_64/ /home/www/html/repos/centos-stream/8/opstools/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/storage/x86_64/ /home/www/html/repos/centos-stream/8/storage/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/virt/x86_64/ /home/www/html/repos/centos-stream/8/virt/x86_64/
sudo rsync -avz --progress rsync://mirror.umd.edu/centos/8-stream/virt/x86_64/ /home/www/html/repos/centos-stream/8/virt/x86_64/

echo "Docker Repository download"
sudo rsync -avz --progress rsync://download.docker.com/linux/centos/8/x86_64/ /home/www/html/repos/docker/linux/centos/8/x86_64/


# 4. Create the repository metadata. After you have all the packages, you need to create the repository metadata. You can use the "createrepo_c" command for this:
# sudo createrepo_c /var/www/html/repos/centos/8/BaseOS/x86_64/os/
# ************** Note: **************
# if you move files and or change the directory paths and time after running the below command you will have to re run the command to update the sqlite db with the proper paths.
sudo createrepo_c /home/www/html/repos/centos-stream/8/

# Using teh /home directory will require it to be added to the apache conf.d file

# Add the follwing code to the end of the  /etc/httpd/conf/httpd.conf
# vim /etc/httpd/conf/httpd.conf
#change DocumentRoot "/var/www/html" >> DocumentRoot "/home/www/html"

#<Directory /home/www/html/repos>
#    Options Indexes FollowSymLinks
#    AllowOverride None
#    Require all granted
#</Directory>

# test the file formate is correct by running the below command
#apachectl -t

# Change ownership
sudo chown -R apache:apache /home/www/html/repos/

# Change permissions on the directories
sudo find /home/www/html/repos/ -type d -exec chmod 755 {} \;

# change permissions on the files
sudo find /home/www/html/repos/ -type f -exec chmod 644 {} \;

# update the content path. 
sudo chcon -R -t httpd_sys_content_t /home/www/html/repos/
sudo semanage fcontext -a -t httpd_sys_content_t "/home/www/html/repos(/.*)?"
sudo restorecon -Rv /home/www/html/repos/


#5. Configure the firewall. You will need to allow HTTP traffic to your server:
sudo firewall-cmd --add-service=http --permanent
sudo firewall-cmd --reload

#6. Start and enable the HTTPD service. This will start the Apache HTTP server and set it to start on boot:
sudo systemctl start httpd
sudo systemctl enable httpd
#sudo systemctl restart httpd
#End of VM1 Setup
