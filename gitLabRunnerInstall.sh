#!/bin/bash

# Install curl and other necessary tools
sudo yum install -y curl

# Add GitLab's official repository
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash

# Install GitLab runner
sudo yum install -y gitlab-runner

# Check if gitlab-runner is installed and its version
gitlab-runner --version

# Register the GitLab runner
echo "Please follow the prompts to register the GitLab Runner with your GitLab instance."
sudo gitlab-runner register \
--non-interactive \
--url "http://10.0.0.167/" \
--registration-token "glrt-nUQ9AqBQbXzdWoinqSEv" \
--executor "shell" \
--description "vmscripts" \
--tag-list "scripts" 

